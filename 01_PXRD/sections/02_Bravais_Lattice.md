# 02 | Crystal Systems and Lattices

**Word count:** <span id="word-count">0</span> words<br>**Reading time:** <span id="read-time">0</span> minutes

The unit cell describes the fundamental repeat unit and is useful as starting point to think about the structure of crystals from a local perspective. To make the work of describing an ideally infinite periodic structure manageable, it is useful to conceptually project a _lattice_ onto the real crystal structure to serve as a guide to discuss the dimensions and symmetry independent of the particular locations of individual atoms.

In a infinitely periodic system we can always find a set of points that are structurally indistinguishable from each other both locally and relative to the location of every other object in the crystal. This set of _lattice points_ is called _crystal lattice_. For example, in the 1D periodic waveform shown in {numref}`beat-animate`, in orange are shown a set of lattice points. It doesn't matter exactly where the lattice is superimposed the points remain mutually indistinguishable regardless of relative position of the periodic structure. In 1D, the distance between these points is the only lattice parameter and the content between points describe a unit cell. A lattice is not a physical object but a conceptual tool we can use to describe the symmetry and periodicity of a crystal structure.

```{raw} html
:file: assets/beat-animate.svg
```

```{figure} assets/beat-animate.svg
:height: 0px
:name: beat-animate

(Click image) A Lattice is a set of lattice points (orange) that can be superimposed on a periodic structure. In an infinite periodic structure all lattice points are perfectly indistinguishable from one another. 
```

## Crystals systems

The term crystal system is synonymous with coordinate system. The crystal system is just the coordination system of a crystal. There are only **four** **2D crystal systems** and **seven 3D crystal systems**. Each is distinguishable from eachother by comparing the equivalency of unit cell lengths and whether the unit cell angles take the special values of 90° or 120°. 

## 2D crystals
### The four 2D crystal systems

In 2D there are 4 crystal systems. Recall a crystal system describe a coordinate system that be can used to define the positions of atoms in a crystal. The _square_ system is formed when the two basis vectors are orthogonal and the same length. The square system is equivalent to the familiar Cartesian coordinate system scaled such that one unit of translation is equivalent to length of the side of the square unit cell. The _rectangular_ system is also composed of two orthogonal basis vectors (all angles are 90°) but the sides of the cell are not the same length.  In the hexagonal crystal system the angle between the basis vectors, $\gamma$, is 120° and all the sides of the unit cell are the same length. In an _oblique_ crystal system the cell angles are neither 90° nor 120° and the unit cell lengths can take any value. The four crystal systems are shown in {numref}`2D-Lattices`. In an _oblique_ crystal system the basis vectors are not orthogonal. These constraints fore each crystal system are summarized in {numref}`crystal-systems-table-2D`.


```{figure} assets/2D-Lattices.svg
:name: 2D-lattices

The five possible lattices in two dimensions. Regions shaded in orange show alternative tilings. Note rectangular-C is a special case of an oblique-P.
```

### The five 2D crystal lattices

A lattice type is described by a combination of (1) a crystal system and (2) a _centering_. The centering of a lattice describes the number and location of lattice points per unit cell. All lattices have a set of lattice points at the four vertices of the unit cell. If these are the only lattice points in the unit cell then the lattice is _primitive_ and symbolized as the letter $p$. In 2D all lattices that have an oblique, square, or hexagonal crystal system are primitive. However, there is a special case of an oblique-$p$ lattice, {numref}`2D-Lattices`, where it is possible to draw a larger unit cell with twice the area where $\gamma = 90°$ and calling this new lattice rectangular-$c$. Doing so allows us the convenience of only describing two lattice parameters $a,\, b$ instead of three and also results in a more intuitive orthogonal coordinate system.

```{note}
It is _always_ possible to describe a centered lattice as a lower symmetry primitive lattice with a smaller unit cell. Doing so is advantageous in electronic structure calculations since it reduces the number of atoms and electrons that need to be calculated.
```



```{list-table} Summary of the four crystal systems, their allowed centerings, and lattice parameters.
:name: crystal-systems-table-2D

* - Crystal System
  - Lattice parameters
  - Constraints
  - Allowed cell centerings
  - Point symmetry at lattice points
* - Oblique
  - $a, \,b,\, \gamma$
  - $a \neq b, \gamma \neq 90°, 120°$
  - $P$
  - $2$
* - Rectangular
  - $a,\, b$
  - $a \neq b, \gamma = 90°$
  - $P,\, C$
  - $2mm$
* - Square
  - $a$
  - $a = b, \gamma = 90°$
  - $P$
  - $4mm$
* - Hexagonal
  - $a$
  - $a = b, \gamma = 120°$
  - $P$
  - $6mm$

```

## 3D crystals

Where there were 4 crystal systems in 2D there are 7 in 3D. While there were only 5 lattice types in 2D, there are 14 lattice types in 3D. The lattices of three dimensional crystals can be constructed by stacking the 2D lattices. By stacking 2D square lattices such the the distance between each 2D square lattice layer is the same as the length of the square unit cell a cubic unit cell is formed and the lattice points described a _cubic_ crystal system. If the distance between the layers of 2D square lattices is different from the length of the square unit cell then a _tetragonal_ crystal system is formed. The unit cell in a tetragonal system has the shape of a square prism. 

By stacking rectangular 2D lattices is doesn't matter what the distance between 2D layers are; so long as all the angles in the new 3D unit cell are 90° then the lattice points describe an _orthorhombic_ crystal system. The shape of a the unit cell in an orthorhombic system is a rectangular prism.

So far when stacking 2D lattices the layers have always been perfectly eclipsed but we could still form a perfectly valid periodic lattice if the layers were offset. 
If the offset angle is not 90° it doesn't matter if the layers are square or rectangular a _monoclinic_ crystal system is formed. The shape of the unit cell is a parallelepiped with four rectangular faces and two parallelogram faces.

The _triclinc_ crystal system is formed by stacking 2D oblique lattices with an offset angle that is not 90°. The lowest symmetry crystal system is the _triclinic_ crystal system. The unit cell in a triclinic system is a parallelepiped with no square or rectangular faces. 





### The seven 3D crystal systems

```{raw} html
:file: assets/lattices-1.svg
```

```{figure} assets/lattices-1.svg
:height: 0px
:name: lattices-1

(Click image) Relationships between five of the seven crystal systems: cubic, tetragonal, orthogonal, monoclinic, and triclinic animated by descents in symmetry.
```

So far we've covered 5 of the 7 crystal systems. The remaining two crystal systems are the hexagonal and trigonal systems which have other special relationships between their unit cell lengths and angles. If the offset angle between stacks layers of 2D hexagonal lattices is 90° the a 2D _hexagonal_ lattice is formed. A hexagonal unit cell has the shape of a hexagonal prism. At in the case of 2D hexagonal lattice the angle between the $a$ and $b$ sides of the unit cell is 120°. The other two angles are 90°. Hexagonal crystals must have 6-fold rotational symmetry. 

All _trigonal_ crystal systems have 3-fold rotational symmetry. The easiest way to form a trigonal unit cell is to stretch a cube along the diagonal such that all the sides are still the same length and all the unit cell angles are the same (but not equal to 90° anymore), {numref}`lattices-2`. The resulting unit cell is a rhombohedron. 

```{raw} html
:file: assets/lattices-2.svg
```

```{figure} assets/lattices-2.svg
:height: 0px
:name: lattices-2

(Click image) Relationships between the cubic and trigonal crystal system by elongation of the cubic unit cell along the diagonal (dashed line). The resulting trigonal lattice is shown in the rhombohedral setting. The 3-fold rotational symmetry axis is along the dotted line.
```



```{figure} assets/Rhomb-Hex-setting-trig-lattice.svg
:name: lattices-3
:width: 400px

In the trigonal crystal system a rhombohedral cell can either be describe as either a _rhombohedral_ lattice (grey circles) with the unit cell parameters $a,\, \alpha$ or in a hexagonal setting (orange circles) with the lattice parameters $a,\, c,\, \gamma = 120°$. Note the volume of the hexagonal setting of the rhombic cell is 3 times the volume of the primitive rhombohedral cell. There are also trigonal cells (3-fold rotation symmetry crystals) that can only be described in the hexagonal setting.
```


### The 14 Bravias lattices

The 14 standard 3D lattice types are called the _Bravais lattices_. All possible periodic arrangements of atoms in crystals must tile space by pure translation of one of these 14 Bravais lattices. The 14 Bravais lattices are shown in {numref}`bravais`. _A Bravais lattice is a combination of a crystal system and a centering_. Just like in 2D is is sometimes useful to draw larger that can be described with fewer lattice parameters (high symmetry) but including additional lattice points inside the unit cell. In 3D there are 4 different types of centering. Not all centerings simplify the number of lattice parameters for every crystal system. Only certain combinations of crystal system and centering can do this. 

The five types of centering are $P$ primitive, $I$ body centered, $F$ face centered, $R$ Rhombic, and $C$ C-face centered. The $P$ centering is the same as the primitive centering in 2D. Body centering means there is an additional lattice point at the center of the unit cell. Face centering means there is an additional lattice point at the center of each of the 6 faces of the unit cell. $C$-centering means there is an additional lattice point on two faces of the unit cell (those described by the $a,b$-plane).

The $I$ centering is the same as the body centered centering in 2D. The $F$ centering is the same as the face centered centering in 2D. The $A$ centering is the same as the A-face centered centering in 2D. The $C$ centering is the same as the C-face centered centering in 2D.

```{figure} assets/Bravais.svg
:name: bravais

The 14 Bravais lattices.
```


```{list-table} Summary of the seven crystal systems and their lattice parameters
:name: crystal-systems-table

* - Crystal System
  - lattice parameters
  - Constraints
  - Allowed cell centerings
  - Point symmetry at lattice points
* - Triclinic
  - $a, \, b, \,c,\, \alpha, \, \beta, \, \gamma$
  - none
  - $P$
  - $1, \bar{1}$ 
* - Monoclinic
  - $a, \, b, \,c,\, \beta$
  - $\alpha = \gamma = 90°$
  - $P, \, C$
  - $\frac{2}{m}$ 
* - Orthorhombic
  - $a, \, b, \,c$
  - $\alpha = \beta = \gamma = 90°$
  - $P, \, A, \, B, \, C, \, I, \, F$
  - $mmm$ 
* - Tetragonal
  - $a, \,c$
  - $a=b;$<br>$\,\alpha = \beta = \gamma = 90°$
  - $P, \, I$
  - $\frac{4}{m}mm$ 
* - Cubic
  - $a$
  - $a=b=c;$<br>$\,\alpha = \beta = \gamma = 90°$
  - $P, \, I, \, F$
  - $m\bar{3}m$ 
* - Trigonal
  - $a, \alpha$ <br>or<br>$a,c$ (on hexagonal axes)
  - $a=b=c;$<br>$\,\alpha = \beta = \gamma \neq 90°$ or <br> $a=b\neq c;$<br>$\,\alpha = \beta=90°,\, \gamma = 120°$
  - $P, \, R$
  - $\bar{3}m$ 
* - Hexagonal
  - $a,c$ 
  - $a=b\neq c;$<br>$\,\alpha = \beta=90°,\, \gamma = 120°$
  - $P$
  - $\frac{6}{m}mm$ 
  ```
