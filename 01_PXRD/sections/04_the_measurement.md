# 04 | Measuring samples


## Outline

Write a short paragraph with helpful images to explain how to measure PXRD samples.

1. Prepare a dry powder sample
2. If the sample is not a fine powder then grind it up in a mortar and pestle
3. Take a glass plate add a thin layer of grease
4. Tap a spatula of powder to put a small amount of sample on the glass plate (~5 mg, 5 mm diameter circle)
5. Turn the glass plate vertical and tap to knock off loose powder. Warning. Loose powder can irreversibly damage the instrument!
6. Add the sample to the rack. 
7. Put the sample in the instrument, write down the sample changer position. 
8. Load all the samples you have at the same time. 
9. Set up the measurement and the file paths for the data for every sample
10. Initial screens should run ~2-5 minute scans
11. Export the data to an xy file for analysis



