# 03 | Powder X-ray Diffraction

**Word count:** <span id="word-count">0</span> words<br>**Reading time:** <span id="read-time">0</span> minutes


## Introduction

So far, we've developed a framework for describing structure of crystalline matter. Using a unit cell we can describe the positions of a small number of atoms in a crystal and then by applying translational symmetry operations we can describe the positions of all the atoms in the crystal. But how are these atomic positions determined in the first place? 

X-ray diffraction is arguably the most important analytical method in all of chemistry. It is used to determine the atomic structure of everything including small molecules, proteins, viruses, DNA, nanoparticles, minerals, metals, alloys, ceramics, semiconductors, etc. At the foundations of chemistry lies the hypothesis that the properties of matter are dictated by the arrangement of atoms in space. **X-ray diffraction is by far the most powerful method we have for the determination of atomic structure**. It is essentially the only experimental method we have to determine the 3D coordinates of atoms in a molecule or solid. 

There are many advanced methods in X-ray diffraction and scattering. Here we will focus on the most common and valuable method: the diffraction of X-rays by crystalline solids. All of our knowledge of the precise structure of small molecules, proteins, and viruses are determined by first crystallizing the compounds as a solid sample and then measuring the X-ray diffraction pattern. Over two dozen Nobel prizes have been awarded for advances in X-ray diffraction and crystallography. In comparison NMR spectroscopy, an incredibly powerful method in organic chemistry and medical MRI, has only been awarded four Nobel prizes.

## X-rays

In order to understand X-ray diffraction we first need to know a little about X-rays. X-rays are a form of electromagnetic radiation. They are similar to visible light but have a much shorter wavelength, 0.1–10 nm. Visible light is greater than 200 nm. When discussing X-ray's we'll discuss both their wavelengths, in unite of Angstroms, and their energy, in units of electron volts (eV). The energy of an X-ray is inversely proportional to its wavelength. The shorter the wavelength the higher the energy. The energy of an X-ray is related to its frequency by the equation $E = \frac{hc}{\lambda}$ where $h$ is Planck's constant and $c$ is the speed of light, and $\lambda$ is the wavelength. The constant $hc=12.4$ keV Å.

### X-ray sources

X-rays are produced by accelerating electrons to high energies within a vaccum tube and then colliding them with a metallic copper target. The electrons are accelerated by a high voltage (~40,000 V). After the electrons collide with the copper atoms in the target, the energy of the electrons is converted into X-rays {numref}`xray-source`. 

```{figure} ./assets/xray-source.svg
:name: xray-source
:width: 500 px

A schematic of a rudimentary X-ray tube. A large voltage is applied across electrodes sealed inside a vacuum tube. The negative electrode is a tungsten filament that emits electrons when heated. The electrons are accelerated towards copper target by the applied voltage. The electrons collide with the copper target and emit Bremsstrahlung radiation and "characteristic X-rays" of atomic copper. 
```

As the electrons decelerate they emit broad spectrum X-ray's called _Bremstrahlung radiation_. The higher possible energy _Bremstrahlung radiation_ that can be produced is equal to the applied voltage times the elementary charge of the electron. In the case shown in, {numref}`xray-source`, the maximum possible energy of an emitted X-ray is 40,000 eV with a wavelength of 0.31 Å. 

The electrons also collide and induce other electronic transitions within individual copper atoms. As these excited atomic states relax back to the ground state, X-rays with very specific (quantized) energies are emitted. These are called _characteristic X-rays_. 

Characteristic X-rays are useful for X-ray diffraction. The Bremstrahlung radiation is filtered out. As a general rule, a good filter should remove all X-ray's except for those with a single specific wavelength; in the case of copper, the characteristic X-ray with a wavelength of 1.54 Å are the most intense and are assigned to the electronic transition Cu-$K\alpha_1$ which is an electronic transition from the $2p$ orbital to the $1s$ orbital. The broad spectrum of Bremstrahlung radiation and other characteristic X-rays are removed by a filter. The filter is usually a thin sheet of metal foil. In the case of copper a nickel foil has an ideal X-ray absorption spectrum to removes all X-rays except for Cu-$K\alpha_1$. Generally a good filter will the elements with an atomic number about one less (to the left of the periodic table) than that of the target in the X-ray tube. Shown in {numref}`xray-spectra` is a typical the emission spectrum from a 15 kV overlaid with the absorption spectrum for a metallic Ni foil.

```{figure} ./assets/xas-cu-source.svg
:name: xray-spectra
:width: 400 px

The emission spectrum of a 15 kV X-ray tube overlaid with the absorption spectrum of a nickel foil. The emission spectrum is the sum of the Bremstrahlung radiation and the characteristic X-rays of copper. The two sharp peaks are characteristic radiation peaks Cu-$K\beta$ (left) and Cu-$K\alpha$ (right). The absorption spectrum of the nickel foil removes most X-rays except for the Cu-$K\alpha_1$ characteristic X-ray. Figure is adapted from [Palmer and Ladd, 2003](https://doi.org/10.1007/978-1-4615-0101-5).

```

# X-ray diffraction is just wave interference

As with all light, X-ray are electromagnetic waves. If a beam of collimated X-rays pass through a small aperture the X-ray will behave as a point source and emit lights in all directions on the opposing side of the aperture. The intensity of the light will diminish in accordance with the inverse square law. If we place a second aperture next to the first, at a distance similar to the wavelength of the incident light, then light will pass through both apertures and interfere with each other on the opposing side. At some locations this interference will be contrutive (their intensities will add to gether) while in other locations the interference will be destructive (their intensities will cancel each other out). 

The pattern of constructive and destructive interference will depend on the wavelength of the light and the distance between the two apertures. This relationship is described by the equation $d \sin \theta = n \lambda$ where $d$ is the distance between the two apertures, $\theta$ is the angle of the diffracted light, $n$ is an integer index, and $\lambda$ is the wavelength. 

```{figure} ./assets/double-slit.svg
:name: double-slit-intensities
:width: 500 px

As collimated monochromatic light passes through two slits each aperture acts as a point source emitted a hemisphere of light. Teh propagating electromagnetic waves from the two slits interfere with each other (a). The interference pattern can be modeled if the geometry of the slits is known. The intensity of the light is greatest at the center and the spacing between the peaks can used to determined the spacing between the two slits as $d=\frac{n\lambda}{\sin\theta}$ (b)
```

With only two slits the pattern is very broad and the intensities are weeks. By adding more slits the pattern becomes more narrow and the intensities become stronger, {numref}`grating-intensities`. This is the basis of the diffraction grating. Provided all of the slits are equally spaced then the space can again be determined from the experiment as $d=\frac{n\lambda}{\sin\theta}$. 

```{figure} ./assets/Grating.svg
:name: grating-intensities
:width: 300 px


A diffraction grating with many equally spaced slides provide a sharper, better resolved, and more intense diffraction pattern
```

When X-ray rays interact with atoms they are similarly scattered and produce analogous interference patterns. The scattering of X-rays by atoms is called _X-ray diffraction_. The diffraction pattern is a function of the arrangement of atoms in the crystal. The symmetry of the diffraction pattern is the same as the symmetry of the diffracted structure. 

```{figure} ./assets/patterns.jpg
:name: 2d-diffraction
:width: 600 px

Simulated diffraction molecules for an diiodide system, a line of 10 equally spaced iodide atoms, benzene face on, benzene edge on, a 3x3 Ni cluster, and and experimental single crystal diffraction image (composited precession photograph). Note the symmetry of the diffraction pattern matches the symmetry of the molecule. 
```

# The X-ray diffraction experiment

In order to measure the X-ray diffraction (i.e. interference) pattern of a crystal we need a monochromatic source of X-rays, a crystalline sample, and a detector. Using the X-ray source shown in {numref}`xray-source` we can can extract monocromatic X-rays using using a Ni foild to filter all wavelengths except for the Cu-$K\alpha$ line. The X-rays from a radial source to a parallel source using a _collimator_. The X-rays are then directed at a sample. Within the sample X-rays are diffracted at all angles. The diffracted X-rays are then collected by a detector. X-ray detectors used to be photographic films but are now almost exclusively pixel arrays. The most advanced and the most common you'll encounter on campus are pixel arrays of semiconducting photon counters. The basic Debey-Scherrer geometry is shown in {numref}`xray-geometry`. 

```{figure} ./assets/Debey-scherrer.svg
:name: xray-geometry
:width: 750 px

The Debey-Scherrer geometry for X-ray diffraction. X-rays are emitted from a point source and are collimated into a parallel beam. The parallel beam is directed at a sample. The diffracted X-rays are collected by a detector. 
```

## Reflexion planes and the Bragg Equation

In a crystal when X-ray scatter off the electron cloud of an atom the incident radiation can be considered to radiate from the atom as a point source. Much like the point sources in the double slit experiment the incident radiation from one atom (aka scattering center) will interfere with the incident radiation from another atom. The interference pattern will depend on the wavelength of the incident radiation and the distance between the scattering centers. Because crystal are highly ordered periodic structures many of these distances are the same resulting in what is called coherent scattering. As in teh case of the diffraction grating above coherent scattering results in sharp and intense diffraction peaks also called _Bragg Peaks_. 

Within a 3D single crystal, unlike a 2D plane we can imagine there to be a great many planes of atoms all of which may yield a different set of diffraction peaks. In 1915 the Braggs were awarded the Nobel Prize in physics for developing a simple model for indexing these complex diffraction patterns and relating the angle of diffraction to the unit cell parameters of a crystal. 

Bragg's law imagines a crystal as being composed of many sets of planes of lattice points from which X-rays are reflected. A geometric construction of Bragg's law is shown in {numref}`bragg-equation`. In actuality the X-ray are scattered by the electron cloud of the atoms. These _reflexion planes_ are not physical planes and they are so called reflexion planes to emphasize the point that they are only a mathematical construct.

Given an array of lattice points we can can connect the points to form sets of equally spaced parallel planes separated by a distance $d$. As shown in {numref}`bragg-equation`, when a source of X-rays reflects off of these planes the light reflected off planes deeper into the crystal must travel further than the light reflected off of planes closer to the surface. The difference in path length is $2d\sin\theta$. If the path length difference is an integer multiple of the wavelength of the X-rays then the waves will interfere constructively and a diffraction peak will be observed. This is condition for constructive interference is summarized by Bragg's law. 

$$
2d\sin\theta=n\lambda
$$

Note that the distance between the planes $d$ appears to be related to the unit cell parameters of the crystal. By measuring the diffraction angle wgere peaks appear on the detector, $\theta$, for many different sets of reflexion planes we can mathematically determine the unit cell parameters of the crystal.


```{figure} ./assets/bragg-equation.svg
:name: bragg-equation
:width: 500 px

The Bragg equation relates the angle of diffraction to the unit cell parameters of a crystal. 
```
### In the lab we measure 2$\theta$

A the laboratory scale the crystal is very small (less than 1 mm) and the distance between the X-ray source and the detector is large (~30-100 cm). When measuring these angles it is convenient to measure the diffraction angle for a detected peak by having the path of the undiffracted beam set along the $x$-axis. Such that the angle the measured between the undiffracted X-ray beam and the diffracted peak is twice the angle $\theta$ in Bragg's law. This experimenta geometry shown in {numref}`bragg-equation` is called the _Debye-Scherrer_ method or a transmission geometry. You will almost always see Powder Diffraction patterns reporting diffraction angles as peak intensity versus $2\theta$. In order to use these angles in Bragg's law we must always divide them by 2.

Another experimental geometry is the Bragg-Brentano (reflection-geometry) which is common for routine powder X-ray diffraction analysis. This geometry is good for powders because it is possible to measure the diffraction patter without rotating the sample or suspending free standing sample in the path of the beam. Instead the sample is placed on a flat surface the te source and detectors rotate around the sample. 

## Experimental PXRD Patterns

Experimentally there are primarily two types of diffraction experiment single crystal and powder. In a single crystal experiment all of the atoms are arranged in a nearly perfect 3D grid that results diffraction _spots_ on the surface of the detector whose positions are dependant on the crystals orientation with respect to the X-ray beam and the distance between the crystal and the detector. Shown in {numref}`single-crystal` is a single crystal diffraction pattern. 

```{figure} ./assets/Ni-scxrd.png
:width: 500 px
:name: single-crystal

A simulated single crystal diffraction pattern shown as spots colored by intensity. The grey circles indicates what a powder diffraction pattern would look like for a large number of randomly oriented crystals. The Bragg angle $2\theta$ is calculated using the distance between the diffracted spot and the center of the detector as well as the distance between the crystal and the detector.
```

In a powder diffraction experiment there are ~100,000 very small single crystals in the beam path at the same time. These tiny crystals are randomly oriented. Otherwise the scenario is the same at above for a single crystal except instead of seeing a single spot at diffraction angle for $2theta$ from the center of the detector there will a very large number of spots forming a circle due to the _orientational averaging_ of many crystals in the beam. If there are a very large number of randomly oriented crystals a continuous ring of diffracted X-ray intensity will be observed. Shown in {numref}`powder-pattern` are two experimental powder diffraction patterns. The first shows a mixture of a few large single crystals with many smaller crystals. The second shows a powder pattern for a very fine powder. The first patter is considered low quality because is extremely difficult to analyze data from a mixture of single crystals and powder. Generally, either one large single crystal or a very fine homogenous powder are needed to extract useful information from a diffraction pattern. 



```{figure} ./assets/pxrd_expt.jpg
:width: 500 px
:name: powder-pattern

Experimental powder diffraction patterns. The first pattern is a mixture of a few large single crystals (spots) with many smaller crystals (circles). The second shows a powder pattern for a very fine powder.
```


Normally in the case of powder X-ray diffraction the 2D data can be simplified by integrating the intensities around the circles to produce a 1D plot of intensity versus $2\theta$. This is shown in {numref}`powder-pattern-line` without significant loss of quantitative diffraction information (i.e. diffraction angle, and diffraction intensity). 

```{figure} ./assets/pxrd_expt_integrated.png
:width: 300 px
:name: powder-pattern-line

Integrated powder diffraction pattern from the right image in {numref}`powder-pattern`. Unit cell and X-ray structure solutions can be modeled and fit to this simplified 1D dataset.
```

## Qualitative Analysis of PXRD Patterns

The crystallinity of a PXRD pattern is determined by considering the relative intensity of peaks or the signal to noise ratio for a roughly equal amount of sample, X-ray source intensity, and detector sensitivity/collection time. A highly crystalline sample will have very sharp peaks with high intensity. A poorly crystalline sample will have broad peaks with low intensity. Amorphous materials will have no peaks. For example in {numref}`crystallinity` is shown an example of the PXRD pattern for glass which is amorphous compared to that of gold which is highly crystalline. 

```{figure} ./assets/crystallinity.svg
:width: 750 px
:name: crystallinity

PXRD patterns for three different materials. The glass is amorphous, the gold is highly crystalline with a small unit cell, and the crystallized protein hemoglobin has an extremely large unit cell. The baselines for the three powder patterns are offset for clarity.
```


From Bragg's law we can also qualitatively comment on the relative size of a compounds unit cell. Since $2d\sin\theta=n\lambda$ it we have a lot of peaks at very low angle then we can infer that the unit cell must be large. If we have a lot of peaks at very high angles then we can infer that the unit cell must be small.


Crystallinity can also be compared between sample of the same compound. This is useful when using powder X-ray diffraction to optimize crystallization conditions to obtain large higher quality crystals. Again, sharper more intense peaks indicate a more crystalline sample. {numref}`crystallinity-peak-width` simulate how a powder diffraction pattern may change as the size of the crystals in the powder become smaller <100 nm in dimensions or the crystal structure becomes more disordered or strained. Notice in the figure how the peak positions do not change but the peak become broader and the signal to noise ratio decreases as the peaks become less intense.

```{figure} ./assets/crystallinity-peak-width.svg
:width: 750 px
:name: crystallinity-peak-width

Simulated PXRD patterns for penicillin as the size of the powder's crystallites become smaller and more disorderd going from highly crystalline (blue) to poorly crystalline (red). The baselines for the three powder patterns are offset for clarity.
```

### Periodic Trends with Bragg's Law

By selecting a more a set of crystalline materials that have the exact same arrangement of atoms in space but with different elements periodic trends in ionic radius can be directly determined from powder diffraction data. This is exactly how all the know ionic radii have been determined. That is by measuring bond distances in crystals using X-ray diffraction. {numref}`ionic-radii` shows the ionic radii of the halides change X-ray diffraction pattern for a set of cesium salts. Iodide is expected to be the largest and since all the crystal structures are the same that means that CsI will have the largest unit cell and therefore typically larger d-spacings and thus smaller diffraction angles.

```{figure} ./assets/pxrd-periodic-trend.svg
:width: 750 px
:name: ionic-radii

PXRD patterns for a set of cesium salts. As the ionic radius of the halide increases the unit cell size increases and the diffraction angle decreases.
```

## Extracting structure information from PXRD patterns

There are 2 components to a PXRD pattern (1) peak positions and (2) peak intensities. __The peak positions are determined by the unit cell and the peak intensities are determined by the atom positions inside the unit cell.__ In order to more closely analyze these patterns we need to be able to relate an observed peak to a specific set of reflection planes with in a crystal. This is done using the _Miller indices_. 



## Miller Indices of Bragg Reflexion Planes

Miller indices are a set of three numbers that define an infinite set of parallel planes in a crystal. The Miller indices are defined as the reciprocals of the intercepts of the plane with the unit cell axes and have the form $[\frac{1}{a_{int}}\;\frac{1}{b_{int}}\;\frac{1}{c_{int}}]$. In {numref}`miller-indices` is shown a set of planes in a 2D unit cell.  In 2D there are only 2 intercepts and thus only 2 Miller indices. In 3D there are 3 intercepts and thus 3 Miller indices. In the case of the indices [0 1] the planes are all parallel to the a-axis and therefore intercept the a-axis at infinity and the planes intercept the b-axis every $1\times \vec{b}$ therefore the miller indices would by $[\frac{1}{\infty}\; \frac{1}{1}]=[0\;1]$. Next consider the miller index $[2\;0]$. This plane is parallel to $\vec{b}$ so the second number zero, while the planes intercept the a-axis every $\frac{1}{2}\times \vec{a}$. The miller index for this set of planes is therefore $[\frac{1}{1/2} \; \frac{1}{\infty}]=[2\;0]$. If this this difficult to visualize try drawing the coordinate system again with the origin at a lattice point. 

Miller indices in real crystals are always integers and tend to be small numbers between -10 and +10. This is because as the Miller indices get larger the planes get closer together and the diffraction angle becomes to large to be observed.


```{figure} ./assets/XRD-Lattice.svg
:width: 500 px
:name: miller-indices

Miller indices for a set of planes in a unit cell. The planes are defined by the intercepts of the plane with the unit cell axes. 
```

It takes a little bit of practice to gain an intuition for miller planes in 3D. There are crystallography visualization software packages like Mercury and Vesta that will plot Miller planes on top of crystal structure for you. {numref}`miller-planes` shows a set of Miller planes in a 3D unit cell. Remember that Miller indices define in infinite set of parallel places and to determined the Miller index for the set you need to find the one that intersects the coordinate systems at values that are either less than or equal to 1 or infinity. 

```{figure} ./assets/miller-planes-3d.svg
:width: 500 px
:name: miller-planes

Assignment of Miller planes in 3D. Below only one or a few Miller planes of the infinte set are shown. 

```

## The unit cell determines a peaks diffraction angle

The distance between reflections planes shown in {numref}`miller-planes` is called the _d-spacing_ and is denoted as $d_{hkl}$ and it determines the peak position in an X-ray diffraction pattern according to Bragg's law. By measuring a pattern in recording a set of peak positions the unit cell parameters (cell lengths and angles) can be determined by first indexing the peaks to miller indices and then searching for a self consistent set of unit cell parameters. Doing this is a bit of a puzzle for which there are many different computer algorithms. 

## The atom positions inside the unit cell determine a peak's intensity

Peak intensities are determined by the positions of atoms in the unit cell. A given peak intensity if determined by two factors (1) how strongly the atoms that are intersected by the plane scatter X-ray's and (2) the combination of constructive and destructive interference of X-ray's reflected by different atoms that are intersected by the same plane. The first factor is quantified by the _atomic scattering factor_ which is a function of the element and the diffraction angle. Atomic scattering factors increase monotonically with the number of electrons about an atom (usually equal to atomic number). The second factors is dependant on the 3D geometry of the structure and what other atoms happen it be intersected by the same plane. This is quantified by the _structure factor_ which is a function of the unit cell parameters, the miller indices, and the atom positions in the unit cell. 

An example of how intensities changes with chemical composition is shown in {numref}`intensity-variation`. In this case the (1 1 1) planes as drawn intersects only the anions but exactly half way in between the (1 1 1) planes is an equivalent set of planes with an identical d-spacing that intersects only the cations. This second set of planes exactly halfway in between the first set will scatter X-ray's perfectly out of phase with the first. However for KF and KBr a strong peak is still observed because the atomic scattering factor for K<sup>+</sup> is significantly different than those for F<sup>–</sup> and Br<sup>–</sup>. However in the case of KCl the atomic scattering factor for Cl<sup>–</sup> is very similar to that of K<sup>+</sup>. Therefore desctructive interference results in a peak intensity very close to zero. 



```{figure} ./assets/intensity-variation.png
:width: 500 px
:name: intensity-variation


Simulated PXRD patterns for a set of rock salt structures. in the structural images the cations are drawn in green and the anions in purple.
```

No consider the (1 1 0) reflection which is completely absent from the patter. Here you can find an identical set of planes with the exactly the same structure and chemical composition half way between (1 1 0) planes. Unlike the (1 1 1) which has weak intensity only for KCl the intensity of the (1 1 0) peak is zero for all three compounds. This is called a systematic absence and is a result of the symmetry of the compound. Note that the (2 2 0) reflection is allowed. This is because (2 2 0) planes would be located at all of the (1 1 0) plane locations drawn in {numref}`intensity-variation` as well as at all of the planes of atoms halfway between the (1 1 0) set. With the help of computational methods more complex structures can be solved using a variety of strategies and algorithms that are beyond the scope of this course and remain under active development. The SHELX software package is one of the most widely used for this purpose and a recent review of the analysis package is among the most cited papers in all of chemistry.[^sheldrick]



## References

[^sheldrick]: Sheldrick, G. M. (2015). SHELXT - Integrated space-group and crystal-structure determination. Acta Crystallographica Section A: Foundations and Advances, 71(1), 3–8. https://doi.org/10.1107/S2053273314026370
