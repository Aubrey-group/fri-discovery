# Data Analysis in Python


## Using Python as a Foundation for Data Analysis

There are many suitable software packages for analyzing instrument data. The Aubrey group prefers using a set of libraries within the python programming language because it can do essentially anything, it is free, and it works on all operating systems. Plotting in python requires you to write a text file that is interpreted to perform calculations and plot data. There is no graphical user interface. 

There are may popular GUI based software packages for data analysis. Some of these are quite good for general plotting, some are very specialized and useful for certain tasks. Here is a list of some some popular plotting software packages used in chemistry. 

### Other Plotting Software

Items in bold can be recommended for this project. You certainly do not need to use all of them but you should have python or one high quality general plotting software package installed. 

**Cross Platform**:
1. [Microsoft Excel](https://www.microsoft.com/en-us/microsoft-365/excel) (free for students, basic plotting, good for manual data entry, bad for reproducibility, not recommended for professional scientific plotting)
2. [**Crystal Diffract**](https://crystalmaker.com/crystaldiffract/) (limited demo available, specialized, excellent for viewing PXRD data, opens many proprietary instrument files)
3. GSAS-II (free, specialized, advanced PXRD analysis)
4. [**Mnova**](https://mestrelab.com/download/mnova/) (free for students, specialized,  NMR analysis, excellent)

**MacOS**:
1. [**Plot2**](https://apps.micw.org/apps/plot2/) (free, general scientific plotting, excellent, professional grade plots)

**Windows**:
1. [**Origin Pro**](https://www.originlab.com/origin) (there is a limited  free version for students, full version is excellent, professional grade plots)
2. [**Spectragraph**](https://www.effemm2.de/spectragryph/about.html) (free, excellent for any UV-vis, NIR, FTIR, Raman, or fluorescence data, opens many proprietary instrument files)

