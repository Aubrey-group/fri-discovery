# Setup Python for Data Analysis

## Learning Python

There are dozens of tutorials available for getting started with python using the tools detailed below. You will need a basic knowledge of the python programming language. Codecademy has a [free course](https://www.codecademy.com/learn/learn-python-3) that is a good place to start. It's not difficult to learn, only takes a couple hours to learn the basic, and is a good skill to have. 

## Lightweight Cloud Based Options

There are a couple of places online where you can use python for free without installing anything. [Google Colab](https://colab.research.google.com/notebooks/intro.ipynb)and [Anaconda](https://www.anaconda.com/products/individual) have online environments where you can do lightweight analysis and make plots. These have the one advantage of working without going through the trials of installing the software on your own system (you won't be able to download this software in an app store). Anaconda cloud has a python tutorial available at login and uses the ubiquitous Jupyter Lab browser interface.

## Local installation

Local installation is recommend in the long term. The cloud based versions have limited storage and compute resources and will charge you for more. They are good for getting started, learning the basics, and non-serious explorations.

The local installation will require a few steps and a few different software packages. We will need to install a text editor, python, and a few python packages. VS Code is a good text editor and is free from Microsoft. It runs on all operating systems. Anaconda is a good python distribution and package manager. The python packages we need to install are listed below. Just follow the installation instructions on their websites. Use the `conda` commands when available and the `pip` commands when they are not.

1. [VS Code](https://code.visualstudio.com/)
2. Install [Anaconda](https://www.anaconda.com/) (or miniconda if you are comfortable with the command line and want to save 1GB of disk space)
3. Install [Pandas](https://pandas.pydata.org), [Numpy](https://numpy.org/), [Scipy](https://scipy.org/), [Matplotlib](https://matplotlib.org/). These can be installed with the command `conda install pandas numpy scipy matplotlib` 
4. Install [Plotly](https://plotly.com/python/getting-started/), [Jupyterlab](https://jupyter.org/install). Go to the links and follow the instructions.
5. In VS Code, go to the extensions tab on the left and install (1) Python extension, and (2) the Jupyter extension

## Using VS Code

VS Code is a text editor that opens folders on you computer so you can quickly access many text files. It is a good tool for writing code and making plots. It is not a GUI based plotting software. You will need to write a text file that is interpreted to perform calculations and plot data.  

To start plotting create a new file with the extension '.ipynb'. This is a Jupyter notebook file. It is a text file that can be opened in VS Code. It is a good idea to create a new folder for each project and put all the data files and analysis files in that folder.


## Using Jupyter Lab

There are a large number of tutorials and videos online introducing ipynb files and using them Jupyter Lab. When you first open the file you need to select a python interpreter. This is the python environment that will be used to run the code. You can use the default python interpreter that comes with Anaconda or create a custom environment if your have multiple projects with different python package requirements.

In an ipynb file there are "code" cells and "markdown" cells. Markdown cells are for writing text and code cells are for writing code. Use the markdown cells to add context and structure to your code cells. The code cells are for python. To make a simple plot you can use the following code. 

```python
import plotly.io as pio
import plotly.graph_objects as go
import numpy as np
import pandas as pd


x = np.linspace(0, 2*np.pi, 1000)
y = np.sin(x)

data = [
    go.Scatter(x=x, y=y)
]
figure_1 = go.Figure(data=data)
pio.write_image(figure_1, 'figure_1.png')
figure_1.show()
```

1. The first four lines import python libraries that we will use for graphing data. 
2. The next two lines define the variables `x` and `y`. Both use numpy (abbreviated np) functions to generate a basic sine function. First 1000 points between 0 and 2$\pi$ are stored as x values. The `np.linspace` function is used to generate 1000 equally spaced numbers. The `np.sin` function is used to generate the `y` values using the `x` variable as input.\
3. The next line starts with `data` defines a python list that contains all the functions that we are going to plot. In this case there is only one function: a scatter plot of `x` vs `y`.
4. Finally we build the figure using `go.Figure` that encodes all the other data about a graph (axes, labels, limits, gridlines...). All these things can be customized and how to do so is explained in the plotly documentation. 
5. Next `pio.write_image` saves the figure to a file with the name `figure_1.png`. You can change the file path to save in a different location using standard file path syntax.
6. Finally `figure_1.show()` displays the figure in the notebook.

Code snippets like these can be saved and just copy and pasted into new notebooks. Usually the only things that need to change are the `x` and `y` variables, files names, and a few style options. The majority of the code can be reused.

## Plotting data from a file

Below is an example of how to plot data from a file. The data file is a text file with two columns of numbers exported from an X-ray diffractometer. The first column is the x values and the second column is the y values. The data file is read into a pandas dataframe (an excel like table) and then plotted. To plot a second data set just dulplicate the pd.read_csv line, change the variable name and filepath then add another `go.Scatter` object to the `data` list.

```python

import plotly.io as pio
import plotly.graph_objects as go
import numpy as np
import pandas as pd

V2O5_Aldrich_223794 = pd.read_csv('./V2O5_Aldrich_223794.xy', sep='\s+', names=['2theta', 'I'] )

data = [
    go.Scatter(x=V2O5_Aldrich_223794['2theta'], y=5*V2O5_Aldrich_223794['I'])
]

figure_2 = go.Figure(data=data)
pio.write_image(figure_2, 'figure_2.png')
figure_2.show()
```