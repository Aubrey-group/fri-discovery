# Chemical Cartography and Materials Discovery


## Project Description

The goal of this project is to engage students to the process of synthetic materials discovery and for them to learn to use the tools and techniques used in the field. The modern process for the discovery of new materials involves the search for of evermore complex and richly structured phases within relatively small regions of a high dimensional synthetic space. We will develop synthetic methods for efficiently searching for reaction conditions that target previously undiscovered. Using traditional methods and thinking tools including Pourbaix diagrams, phase diagrams, reaction mapping, structure mapping, and design of experiments we will to discover new redox active materials relevant to energy storage, magnetism, and coloration.

## Research Experience and Learning Objectives 

In the first phase, students will be introduced to the concept of **reaction space mapping** and learn to set up and run exploratory synthetic reactions targeted at the synthesis of functional materials. Students will learn to structurally characterize the products of these reactions using **infrared absorption spectroscopy**, PXRD and single crystal **X-ray diffraction**, and **UV-*vis*-NIR absorption spectroscopy**.

The reaction spaces will be defined by the stoichiometry of the reactants, the solvent, the total concentration, pH, anion activity, and temperature. Students will be expected to design a set of experiments that will allow them to identify the reaction conditions that lead to the formation of a new products and to rationalize the formation conditions using their knowledge of general chemistry.

### Technical skills gained 

1. The safe handling of hazardous chemicals under unknown and exploratory reaction conditions. 
2. The execution of solution self-assembly and crystallization reactions in the lab.
3. The determination of solubility
4. The use and determination of an E-pH (Pourbaix) diagram
5. Techniques in solvothermal synthesis
6. Prepare and measured samples using powder X-ray diffraction
7. The determination of a crystal unit cell using powder X-ray diffraction
8. Determination and refinement of 3D atomic coordinates using single crystal X-ray diffraction
9. The measurement of infrared absorption spectra
10. The identification of materials and their transformations using infrared absorption spectra
11. Collection of UV-*vis*-NIR using diffuse reflectance spectroscopy.
12. Organization of experimental methods and results in a research lab notebook. 
13. Methods for the systematic search through a very large number of reactions conditions using techniques like design of experiments.
14. Organization of sets of reaction conditions for visualization and statical analysis. 
