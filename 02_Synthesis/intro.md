# Synthesis & Reaction Mapping

1. Partial dissolution and slow cooling
2. Inverse temperature crystallization
3. Slow evaporation
4. Vapor diffusion

## Experimental Design

1. Design of experiments
2. Reaction mapping
3. Data Logging
4. Rapid evaluation of experiments
5. Data analysis
6. Data management