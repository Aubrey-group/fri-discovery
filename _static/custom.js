window.onload = count_words;
window.PlotlyConfig = {MathJaxConfig: 'local'}



// MathJax.Hub.Config({
//     extensions: ["tex2jax.js"],
//     TeX: {extensions: ["mhchem.js"]},
//     jax: ["input/TeX", "output/HTML-CSS"],
//     tex2jax: {
//       inlineMath: [ ['$','$'], ["\\(","\\)"] ],
//       displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
//       processEscapes: true
//     },
//     "HTML-CSS": { fonts: ["TeX"] }
//   });


function count_words() {
  var profile_values = document.getElementsByTagName("section");
  var total_words = 0;
  for (i = 0; i < profile_values.length; i++) {
    total_words += profile_values[i].textContent.trim().split(/\s+/).length;
  }
  document.getElementById("word-count").innerHTML = Math.round(total_words/10)*10;
  document.getElementById("read-time").innerHTML = Math.round(total_words/130)+1;
}
