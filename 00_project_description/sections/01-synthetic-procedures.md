# Synthetic Methods

The preparation of potassium rhodizonate has been previously reported.[^RhodizSynth] We will follow the procedure as previously reported. In previous reproductions of the procedure the role of oxygen is the reaction was not clear and will be investigated. The product will the characterized by PXRD, IR, and UV-vis diffuse reflectance. Importantly croconate and unreacted inositol are likely impurities of the reaction and their spectral signatures and unique diffraction patterns will be used to identify them as potential impurities in the isolated product.


[^RhodizSynth]: Preisler, P. W.; Berger, L. "Preparation of Tetrahydroxyquinone and Rhodizonic Acid Salts from the Product of the Oxidation of Inositol with Nitric Acid" *J. Am. Chem. Soc.* **1942**, *64*, 67–69. https://doi.org/10.1021/ja01253a016

## Materials List

Below is listed a set of chemicals and materials we will need for this project. The list contains a range of common solvents used in the synthesis of metal-organic hybrid materials and metal salts that have previously been used to successfully crystallize rhodizonate salts. 

### Reagents and solvents

- myo-inositol
- nitric acid
- HCl
- water
- methanol
- ethanol
- isopropanol
- DMF
- DMSO
- THF
- DME

### Metal Salts

- Copper Acetate
- Zinc Acetate
- Magnesium Acetate
- Aluminum Acetate
- Sodium Acetate
- Lithium Acetate
- Calcium Acetate
- Copper Nitrate Hydrate
- Zinc Nitrate Hydrate
- Magnesium Nitrate Hydrate
- Aluminum Nitrate Hydrate
- Potassium Carbonate
- Sodium Carbonate
