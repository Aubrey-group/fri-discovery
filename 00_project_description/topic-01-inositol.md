# 01 | Coordination solids of rhodizonate

**Word count:** <span id="word-count">0</span> words<br>**Reading time:** <span id="read-time">0</span> minutes

## Introduction 

The carbon oxides are a class of materials composed of the two elements carbon and oxygen. Two of their structures are relatively stable and they are familiar to every chemist: carbon dioxide ($\ce{CO2}$), carbon monoxide ($\ce{CO}$). Of lesser notoriety are the reduced carbon suboxides including the linear compound [tricarbon dioxide](https://en.wikipedia.org/wiki/Carbon_suboxide) ($\ce{C3O2}$),[^suboxide] [mellitic anhydride](https://en.wikipedia.org/wiki/Mellitic_anhydride) ($\ce{C12O9}$), [hexahydroxybenzene triscarbonate](https://en.wikipedia.org/wiki/Hexahydroxybenzene_triscarbonate) ($\ce{C9O9}$), and [hexahydroxybenzene trisoxalate](https://en.wikipedia.org/wiki/Hexahydroxybenzene_trisoxalate). By somewhat relaxing our rules to include charged carbon oxides that necessarily must be charge balanced by $\ce{H+}$ or a metal cation, we are able to enumerate quite a few more carbon oxyanions the commonest of which is the hydrated product of carbon dioxide: [carbonic acid](https://en.wikipedia.org/wiki/Carbonic_acid) ($\ce{H2CO3}$). Metal salts of the carbonate $\ce{CO3–}$ are among of the most common bases available. However, there are also reduced forms of the carbon oxyanions that are more richly structured and of high symmetry. [Oxalate](https://en.wikipedia.org/wiki/Oxalate) ($\ce{C2O4^2-}$) salts are naturally occurring as in the mineral [weddellite](https://en.wikipedia.org/wiki/Weddellite) ($\ce{CaC2O4·2H2O}$), a component of kidney stones. Others include [squarate](https://en.wikipedia.org/wiki/Squaric_acid) ($\ce{C4O4^2-}$), [croconate](https://en.wikipedia.org/wiki/Croconic_acid) ($\ce{C5O5^2-}$), [rhodizonate](https://en.wikipedia.org/wiki/Rhodizonic_acid) ($\ce{C6O6^2-}$), [tetroxidobenzoquinone](https://en.wikipedia.org/wiki/Tetrahydroxy-1,4-benzoquinone) ($\ce{C6O6^4-}$), [hexoxidobenzene](https://en.wikipedia.org/wiki/Benzenehexol) ($\ce{C6O6^6-}$), and [mellitate](https://en.wikipedia.org/wiki/Mellitic_acid) ($\ce{C12O12^6-}$). 


```{figure} assets/inositol.png
:name: inositol-fig
:width: 400px

Oxidation products of inositol. 
```

Of particular interest for energy storage are those carbon oxides that are redox active and can be of potential use as electrode materials in batteries and related electrochemical cells. The four most redox active carbon oxides can all be derived from the carbocyclic sugar inositol, {numref}`inositol-fig`. The naturally occurring isomer of inositol is *myo*-inositol, a corn product that can be extracted in high yields. By oxidation of *myo*-inositol using concentrated nitric acid the six-carbon rhodizonic acid is forms in solution. The salts of sodium and potassium can then be formed following neutralization of the acid solution with a suitable base like potassium acetate. At higher pH's the reduced form of the compound form preferentially first as tetrahydroxyquinone and at more reducing potentials and higher pH the hexahydroxybenzene form is preferred, {numref}`rhodizonate-fig`. Specific energies nearly double those of commercial lithium ion batteries have been reported for the lithium salts of these materials[^tarascon]. The parent compound rhodizonic acid is susceptible to thermal decomposition that effectively results in a ring contraction to form the planar five membered oxocarbon croconic acid. The redox chemistry of croconic is is comparatively less explored. 



```{figure} assets/rhodizonate.png
:width: 400px
:name: rhodizonate-fig

Reductive half reactions for rhodizonic acid under acidic conditions 
```

## Project plan

Surprisingly very few crystalline materials are known to form as metal salts of there compounds. Crystal structures of rhodizonate are only know for the sodium and potassium salts and powder X-ray diffraction has been reported for lithium[^tarascon] and a few other metal salts.[^TM-salts] Our goal it is determine conditions for the formation of crystalline metal rhodizonate phases, determine their compositions, structures, and electronic properties. 

These materials can be prepared by metathesis with another metal salt. *Metathesis* is a class of reactions that can be described as an exchange of anions between two different metal cations {numref}`metathesis-fig`. In both case not that the metal cation, $M^{n+}$, swaps places with the protons or potassium ions respectively. Based on the stoichiometry of the regents and products presented, what is the formal oxidation state of $M^{n+}$?

```{figure} assets/metathesis.png
:name: metathesis-fig
:width: 400px

Two different metathesis reactions schemes. The first using rhodizonic acid as a reagent and the second using the salt potassium rhodizonate as a reagent. 
```

### Objective 1 | Synthesis of Rhodizonic acid

Rhodizonic acid is a commercially available compound but can be readily synthesized by stirring *myo*-inositol in concentrated nitric acid. The reaction is exothermic and care should be taken to add the inositol slowly to the acid. The reaction is complete when the solution turns a deep red color. The solution can be neutralized with a base like potassium acetate to form the potassium salt of rhodizonic acid. The potassium salt is more stable than the acid and can be stored for longer periods of time. The sodium salt can be similarly prepared by using sodium acetate. The lithium salt can be prepared using lithium acetate, but this product has not been isolated as single crystals suitable for X-ray structure determination.

### Objective 2 | Aqueous stability rhodizonic acid and its salts

There are some discrepancies in the literature regarding the synthesis and stability of rhodizonic acid and its alkali salts with regard to their acid base chemistry, reactivity with air/dioxygen, thermal stability, and redox stability. The goal of this objective is to characterize the known salts of rhodizonic acid and compare the results to the literature. We will use PXRD, SCXRD, TGA, and FT-IR, and titration techniques to determine the solution phase chemistry of this compound. Importantly it is know that under certain conditions Rhodizonic acid decomposed to form croconic acid and evolve carbon monoxide, and sometimes react further to form oxalic acid. The goal of this objective is to determine the conditions under which this decomposition occurs, such that they can be avoided during the synthetic trials we'll use to search for new materials. Further more the highly oxidized rhodizonate is susceptible to reduction to from the products tetrahydroxyquinone and hexahydroxybenzene. The goal of this objective is to determine the conditions under which these reduction occurs, we can take advantage of similar condition during  synthetic trials to search for new materials composed of a metal and rhodizonate in one or these reduced states.

### Objective 3 | Non-aqueous stability rhodizonic acid and its salts

The stability and reactivity of rhodizonic acid in non-aqueous solvent and mixture of aqueous and non-aqueous solvent has not been systematically investigated. In similar fashion to objective 2, we will use PXRD, SCXRD, TGA, and FT-IR, and titration techniques to determine the solution phase chemistry of rhodizonic acid in other solvent systems. 

### Objective 4 | Synthesis and crystallization of new salts of rhodizonic acid

The long term goal of this project is to determine a set of synthetic conditions for the crystallization and isolation (collection of a pure sample) on new materials composed of only rhodizonate and a metal cation. 

## Learning Experiences

With you this project you will learn a balanced set of practical skills common to chemical research particularly with the fields of chemical synthesis and materials discovery. You will learn how to use a variety of laboratory equipment and techniques including:

1. Organizing and maintaining professional laboratory notebooks
2. Handling hazardous chemicals
3. Planning synthetic experiments
4. IR spectroscopy
5. PXRD and SCXRD
6. Analyzing and constructing phase diagrams and reaction space diagrams
7. Managing reaction condition and outcome in a database
8. Statistical analysis and predictive modeling using Design of Experiments




## Footnotes
[^suboxide]: Reyerson, L. H.; Kobe, K. "Carbon Suboxide" *Chem. Rev*. **1930**, *7*, 479–492. https://doi.org/10.1021/cr60028a002
[^tarascon]: Chen, H.; Armand, M.; Demailly, G.; Dolhem, F.; Poizot, P.;  Tarascon, J.M. "From biomass to a renewable $\ce{Li_xC6O6}$ organic electrode for sustainable Li‐ion batteries." *ChemSusChem*, **2008**, *1*, 348-355. https://doi.org/10.1002/cssc.200700161
[^TM-salts]:de Souzaa, H.; Fariaa,  F. L. O.; Scaldinia, F. M.; Dinizb, R.; Edwardsc, H. G. M.; de Oliveira, L. F., C. "Rhodizonate complexes with transition metal ions: A new vision from an old building block" *J. Mol. Struct.* **2021**, *1238*, 130407. https://doi.org/10.1016/j.molstruc.2021.130407

